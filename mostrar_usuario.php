<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mostrar usuarios</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="author" href="humans.txt">
    </head>
    <body>
        <h1>usuarios</h1>
		<table border="1">
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Correo</th>
				<th>Fecha hora de resgitro</th>
				<th>Eliminar</th>
				<th>Actualizar</th>
			</tr>
			<?php 
				include 'coneccion.php';
				$link = conect();
				$stringQuery = "SELECT * FROM usuario";
				$sql = mysqli_query($link, $stringQuery);
				while ($row = mysqli_fetch_array($sql)) {
					$id = $row['id'];
					$name = utf8_encode($row['nombre']);
					echo '<tr>';
					echo '<td>'. $row['id'] . '</td>';
					echo '<td>'. utf8_encode($row['nombre']).'</td>';
					echo '<td>'. utf8_encode($row['correo']).'</td>';
					echo '<td>'. utf8_encode($row['fecha_hora_registro']).'</td>';
					echo '<td>';
					echo '<a href="eliminar.php?id='.$id.'" title="Eliminar a '.$name.'" onclick="return confirm(\'¿Estas seguro?\');">Eliminar</a>';
					echo '</td>';
					echo '<td>';
					echo '<a href="modificar.php?id='.$id.'" title="Modificar a '.$name.'" onclick="return confirm(\'¿Estas seguro?\');">Actualizar</a>';
					echo '</td>';
					echo '</tr>';
				}
			?>
			<caption align="bottom">
				Total Registros:
				<?php 
					$numberRows = mysqli_num_rows($sql);
					echo $numberRows;
				?>
			</caption>
		</table>
		<button>
			<?php 
				echo '<a href="ingresar.php" class="agregar">Ingresar un nuevo registro</a>';
			?>
		</button>
		<?php			
			mysqli_free_result($sql);			
			mysqli_close($link);
		?>

        <script src="js/main.js"></script>
    </body>
</html>